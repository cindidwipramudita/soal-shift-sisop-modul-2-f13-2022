#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#define HOME "/home/rachel"
#define ZIPFILE HOME "/animal.zip"

// Bikin folder baru
void cr_dir(char *nama_dir) {
	
    pid_t pid = fork();
    int status;
    if (pid == 0) {
    	
        char *argv[] = {"mkdir", nama_dir, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}

// Hapus folder
void rm_dir(char *nama_dir) {
	
    pid_t pid = fork();
    int status;
    if (pid == 0) {
    	
        char *argv[] = {"rmdir", nama_dir, NULL};
        execv("/usr/bin/rmdir", argv);
        exit(0);
    }
    wait(&status);
}

//Ekstrak file zip
void extraction(char *path_zip, char *path_extract) {
    
    pid_t pid = fork();
    int status;
    if (pid == 0){
    	
        char *argv[] = {"unzip", "-q", path_zip, "-d", path_extract, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}

void main(){
	
    cr_dir(HOME "/modul2");
    cr_dir(HOME "/modul2/darat");
    sleep(3);
    cr_dir(HOME "/modul2/air");
    extraction(ZIPFILE, HOME "/modul2");

    struct dirent *dp;
    DIR *dir = opendir(HOME "/modul2/animal");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL){
    	
        // simpan nama file ke d_name
        char *d_name = malloc(strlen(dp->d_name) + 1);
        strcpy(d_name, dp->d_name);
        
        if (strcmp(d_name + strlen(d_name) - 4, ".jpg") == 0) {
        	
            char *air, *darat;
            
            char *fullpath = malloc(64);
            strcpy(fullpath, HOME);
            strcat(fullpath, "/modul2/animal/");
            strcat(fullpath, d_name);

            char *cmd = malloc(12);
            char **argv;
            argv = malloc(4 * sizeof(char *));
            for (int i = 0; i < 4; i++) {
                argv[i] = malloc(64);
            }

            // mengecek apakah nama file mengandung kata air, darat, atau tidak
            air = strstr(d_name, "air");
            darat = strstr(d_name, "darat");
            
            if (air) {
                // jika air maka dipindah ke direktori air
                strcpy(cmd, "/usr/bin/mv");
                strcpy(argv[0], "mv");
                strcpy(argv[1], fullpath);
                strcpy(argv[2], HOME "/modul2/air/");
                argv[3] = NULL;
            }
            
            else if (darat) {
                // jika darat maka dipindah ke direktori darat
                strcpy(cmd, "/usr/bin/mv");
                strcpy(argv[0], "mv");
                strcpy(argv[1], fullpath);
                strcpy(argv[2], HOME "/modul2/darat/");
                argv[3] = NULL;
            }
            
            else {
                // jika tidak ada keduanya maka dihapus
                strcpy(cmd, "/usr/bin/rm");
                strcpy(argv[0], "rm");
                strcpy(argv[1], fullpath);
                strcpy(argv[2], "-f");
                argv[3] = NULL;
            }
            pid_t pid = fork();
            int status;
            if (pid == 0) {
                execv(cmd, argv);
                exit(0);
            }
            wait(&status);
        }
    }
    closedir(dir);

    // menghapus folder animal yg kosong
    rm_dir(HOME "/modul2/animal");

    // membaca isi dir darat
    dir = opendir(HOME "/modul2/darat");
    while ((dp = readdir(dir)) != NULL) {
    	
        char *d_name = malloc(strlen(dp->d_name) + 1);
        strcpy(d_name, dp->d_name);

        // pilih file burung dan hapus
        char *bird;
        bird = strstr(d_name, "bird");
        if (bird) {
        	
            char *fullpath = malloc(64);
            strcpy(fullpath, HOME);
            strcat(fullpath, "/modul2/darat/");
            strcat(fullpath, d_name);
            pid_t pid = fork();
            int status;
            if (pid == 0){
                char *argv[] = {"rm", fullpath, "-f", NULL};
                execv("/usr/bin/rm", argv);
                exit(0);
            }
            wait(&status);
        }
    }
    closedir(dir);

    FILE *fptr;
    fptr = fopen(HOME "/modul2/air/list.txt", "w");

    // membaca isi dir air
    dir = opendir(HOME "/modul2/air");
    while ((dp = readdir(dir)) != NULL){
    	
        char *fullpath = malloc(64);
        strcpy(fullpath, HOME);
        strcat(fullpath, "/modul2/air/");
        strcat(fullpath, dp->d_name);

        struct stat fs;
        int r;
        
        r = stat(fullpath, &fs);
        if (r == -1) {
            printf("File Error\n");
            exit(1);
        }
        
        struct passwd *pw = getpwuid(fs.st_uid);

        if (fptr && strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
        	
            char *to_write = malloc(100);

            if (pw != 0)
                strcpy(to_write, pw->pw_name);
            strcat(to_write, "_");
            
            if (fs.st_mode & S_IRUSR)
                strcat(to_write, "r");
            if (fs.st_mode & S_IWUSR)
                strcat(to_write, "w");
            if (fs.st_mode & S_IXUSR)
                strcat(to_write, "x");
            strcat(to_write, "_");
            
            strcat(to_write, dp->d_name);
            
            //revisi dari sini
            char *ada;
            ada = strstr(dp->d_name, "jpg");
            if (ada)
            //revisi sampai sini
            fprintf(fptr, "%s\n", to_write);
        }
    }
    fclose(fptr);
    closedir(dir);
}

