#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdbool.h>
#include <wait.h>
#include <time.h>
#include <dirent.h> 
#include <limits.h>


void download_and_extract(){
    char *links[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
    
    char *file[] = {
                     "characters.zip","weapons.zip" 
                    };

        int status;
        pid_t child = fork();

    if(child < 0){
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 2 ; i ++){
        if(child == 0){
            char *argv[] = {"wget", "-q", "--no-check-certificate", links[i], "-O", file[i], NULL};
            execv("/bin/wget", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }

    for (int i = 0; i < 2 ; i ++){
        if(child == 0){
            char *argv[] = {"unzip", file[i], NULL};
            execv("/bin/unzip", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}


void make_directory(){
    int status;

    if(fork() == 0){
        char *argv[] = {"mkdir", "/home/gacha_gacha",NULL};
        execv("/bin/mkdir", argv);
    }

}

void move_then_delete(){
    pid_t child = fork();
    int status;

    char cwd[PATH_MAX], path[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char current[PATH_MAX];
    char dest[PATH_MAX];

    char *file[] = {"characters", "weapons"};

    for (int i = 0; i < 2; i++){
        strcpy(current, cwd);
        strcpy(dest, "/home/gacha_gacha/"); 
        dir = opendir(current);

        while((dp = readdir(dir)) != NULL){
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
                if(child == 0){
                    char file[PATH_MAX];
                    strcpy(file, current);
                    strcat(file, "/");
                    strcat(file, dp->d_name);
                    
                    char *argv[] = {"mv", file, dest, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
                child = fork();
            }
        }
        closedir(dir);
    }

    strcpy(path, "/home/gacha_gacha/");
    dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "weapons") != 0 && strcmp(dp->d_name, "characters") != 0){

            strcpy(path, "/home/gacha_gacha/");
            strcat(path, dp->d_name);

            if(child == 0){
                char *argv[] = {"rm", "-rf", path, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
            child = fork();
        }
    }
    closedir(dir);
}

int main(){
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    char cwd[PATH_MAX];
    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    int flag = 0;
    while (flag == 0) {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        printf("%d %d %d %d %d\n", tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

        download_and_extract();
        make_directory();
        move_then_delete();

        flag = 1;
    }
}
