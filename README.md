# soal-shift-sisop-modul-2-f13-2022



## Anggota Kelompok 
1. Shaula Aljauhara Riyadi 5025201265
2. Cindi Dwi Pramudita 5025201042
3. Rachel Anggieuli AP 5025201263

## Soal 1

Isi dari folder characters

![](soal1/characters.jpg)

Isi dari folder weapon

![](soal1/weapon.jpg)

## Soal 2
japrun diminta untuk mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. kemudian mengkategorikan setiap poster menurut jenisnya dan membuat folder untuk setiap jenis drama korea yang ada dalam zip, kemudian japrun dimimta untuk memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Di setiap folder kategori drama korea dibuat sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut. 

fungsi untuk membuat direktori
    'void makeDir(char *path) 
    {
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
    }'

fungsi untuk melakukan unzip
    'void unzip(char *path, char *zipFile) 
    {
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"unzip", "-d", path, zipFile, NULL};
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
	}
    }'
fungsi untuk menghapus file yang tidak berguna 
    'void removeDir(char *path, char *dir) 
    {
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
	
		char pathDir[100];
		sprintf(pathDir, "%s%s", path, dir);
	
		char *argv[] = {"rm", "-r", pathDir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
    }

    void removeFile(char *dir, char *fileName) 
    {
	char source[100];
	strcpy(source, dir);
	strcat(source, fileName);

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", source, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
    }

    'void copy(char *src, char *nameOld, char *nameNew, char *dest) 
    {
	char source[100];
	sprintf(source, "%s%s", src, nameOld);
	
	char target[100];
	sprintf(target, "%s%s/%s.png", src, dest, nameNew);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"cp", source, target, NULL};
		execv("/bin/cp", argv);
	} else {
		while(wait(&status) > 0);
	}
    }'
fungsi untuk membuat file txt
    'void makeTxtFile(char *path, char *category, char *name, char *year) {
    char filePath[100];
    sprintf(filePath, "%s%s/data.txt", path, category);

    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id == 0) {
    FILE *writef;
    writef = fopen(filePath, "a");

    fprintf(writef, "%s;%s\n", name, year);

    fclose(writef);

    exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
    while(wait(&status) > 0);
    }
    }'

fungsi untuk melakukan sort file
    'void sortTxtFile(char *path, char *dir) {
    pid_t child_id;
    child_id = fork();
    
    char sortCommand[200];
    sprintf(sortCommand, "sort -t';' -k2 %s%s/data.txt > %s%s/sorted.txt", path, dir, path, dir);

    if (child_id == 0) {
    char *argv[] = {"sh", "-c", sortCommand, NULL};
    execv("/bin/sh", argv);

    exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
    wait(NULL);
    }
    }'

    'void refactorTxtFile(char *path, char *name){
    FILE *readf, *writef;
    
    char readPath[100];
    char writePath[100];
    
    sprintf(readPath, "%s%s/sorted.txt", path, name);
    sprintf(writePath, "%s%s/data.txt", path, name);
    
    readf = fopen(readPath, "r");
    writef = fopen(writePath, "w");

    fprintf(writef, "kategori : %s\n", name);

    char line[100];

    while(fgets(line, sizeof(line), readf)){
    char *breaks = strtok(line, ";");
    fprintf(writef,"\nnama : %s", breaks);

    breaks = strtok(NULL, ";");
    fprintf(writef,"\nrilis : tahun %s", breaks);
    }

    fclose(readf);
    fclose(writef);

	char removePath[100];
	sprintf(removePath, "%s%s/", path, name);
    removeFile(removePath, "sorted.txt");
    }

    void categorize(char *path, char *fileName) {
	char name[50];
	sprintf(name, "%s", fileName);
	
	char *token;
	char *data[3];
	token = strtok(fileName, ";_");
	
	while (token != NULL) {
		for (int i = 0; i < 3; ++i) {
			data[i] = token;
			token = strtok(NULL, ";_");
		}
		
		char *judul = data[0];
		char *tahun = data[1];
		char *kategori = data[2];
		
		// hapus ".png" dari kategori
    char *category;
    category = strstr(kategori, ".png");
    if (category != NULL) {
    int pos = category - kategori;
    sprintf(kategori, "%.*s", pos, kategori);
    }
        
    char dirPath[100];
    sprintf(dirPath, "%s%s", path, kategori);

		makeDir(dirPath);
		copy(path, name, judul, kategori);
    makeTxtFile(path, kategori, judul, tahun);
	}
    }


    'int main() {
	char *pathDrakor = "/home/cindi/shift2/drakor/";
	char *zipFile = "drakor.zip";
	
	makeDir(pathDrakor); 
	unzip(pathDrakor, zipFile);
	
	DIR *dp;
	struct dirent *dirp;
	
	dp = opendir(pathDrakor);
	while ((dirp = readdir(dp)) != NULL) {
	if (strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0) {
			if (dirp->d_type == DT_DIR) {
				removeDir(pathDrakor, dirp->d_name);
			} else {
				categorize(pathDrakor, dirp->d_name);
			}
		}	    	 
	}
	closedir(dp);

	dp = opendir(pathDrakor);
	while ((dirp = readdir(dp)) != NULL) {
	if (strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0) {
	if (dirp->d_type == DT_REG) {
	removeFile(pathDrakor, dirp->d_name);
	} else {
				sortTxtFile(pathDrakor, dirp->d_name);
				refactorTxtFile(pathDrakor, dirp->d_name);
	    	}
		}	    	 
	}
	closedir(dp);

	return 0;
    }'

hasil folder 

![](soal2/folder_soal2.png)

poster di dalam salah satu folder

![](soal2/poster.png)

isi dalam salah satu file.txt

![](soal2/file_txt.png)

## Soal 3
Conan diminta untuk membuat suatu program untuk mengekstrak isi dari animal.zip ke dalam direktori yang nantinya akan dibagi sesuai dengan tinggal hidupnya yaitu darat dan air. Pembuatan direktori 'air' akan dilakukan 3 detik setelah pembuatan direktori 'darat'. Untuk file yang tidak memiliki kata 'darat' maupun 'air' akan dihapus. Pada direktori 'darat', setiap nama file yang memiliki kata 'bird' akan dihapus. Pada direktori 'air' kita akan membuat sebuah file dengan nama 'list.txt' yang nantinya akan kita isi dengan list nama hewan yang ada pada direktori 'air' .


### a. Membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

Fungsi untuk membuat direktori
    'void cr_dir(char *nama_dir) {
        
        pid_t pid = fork();
        int status;
        if (pid == 0) {
        
            char *argv[] = {"mkdir", nama_dir, NULL};
            execv("/usr/bin/mkdir", argv);
            exit(0);
        }
        wait(&status);
    }'

Lalu dipanggil untuk membuat direktori darat dan air dengan jeda 3 detik

    cr_dir(HOME "/modul2");
    cr_dir(HOME "/modul2/darat");
    sleep(3);
    cr_dir(HOME "/modul2/air");



### b. Melakukan extract “animal.zip” di “/home/[USER]/modul2/”

Fungsi untuk mengekstrak file zip
    void extraction(char *path_zip, char *path_extract) {
    
    pid_t pid = fork();
    int status;
    if (pid == 0){
    	
        char *argv[] = {"unzip", "-q", path_zip, "-d", path_extract, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
        }
    wait(&status);
    }

Dipanggil untuk dimasukkan ke dalam direktori modul2

    'extraction(ZIPFILE, HOME "/modul2");'



### c. Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

Dicek dulu apakah terdapat kata 'air' dan 'darat' pada file, lalu dipisah sesuai dengan namanya

    air = strstr(d_name, "air");
    darat = strstr(d_name, "darat");
            
    if (air) {
        // jika air maka dipindah ke direktori air
        strcpy(cmd, "/usr/bin/mv");
        strcpy(argv[0], "mv");
        strcpy(argv[1], fullpath);
        strcpy(argv[2], HOME "/modul2/air/");
        argv[3] = NULL;
    }
            
    else if (darat) {
        // jika darat maka dipindah ke direktori darat
        strcpy(cmd, "/usr/bin/mv");
        strcpy(argv[0], "mv");
        strcpy(argv[1], fullpath);
        strcpy(argv[2], HOME "/modul2/darat/");
        argv[3] = NULL;
    }
            
    else {
         // jika tidak ada keduanya maka dihapus
        strcpy(cmd, "/usr/bin/rm");
        strcpy(argv[0], "rm");
        strcpy(argv[1], fullpath);
        strcpy(argv[2], "-f");
        argv[3] = NULL;
    }



### d. Menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”

Dicek dulu apakah file tersebut terdapat kata 'bird', lalu file akan dihapus

    char *bird;
    bird = strstr(d_name, "bird");
    if (bird) {
        	
        char *fullpath = malloc(64);
        strcpy(fullpath, HOME);
        strcat(fullpath, "/modul2/darat/");
        strcat(fullpath, d_name);
        pid_t pid = fork();
        int status;
        if (pid == 0){
            char *argv[] = {"rm", fullpath, "-f", NULL};
            execv("/usr/bin/rm", argv);
            exit(0);
        }
        wait(&status);
    }

### e. Membuat file list.txt di folder 'air' dan membuat list nama semua hewan yang ada di directory 'air' ke “list.txt”.

Mmbuat file dengan namanya 'list.txt' dan menulis list nama hewan sesuai format pada soal.

    if (fptr && strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
        
        char *to_write = malloc(100);

        if (pw != 0)
            strcpy(to_write, pw->pw_name);
        strcat(to_write, "_");
            
        if (fs.st_mode & S_IRUSR)
            strcat(to_write, "r");
        if (fs.st_mode & S_IWUSR)
            strcat(to_write, "w");
        if (fs.st_mode & S_IXUSR)
            strcat(to_write, "x");
        strcat(to_write, "_");
            
        strcat(to_write, dp->d_name);
            
        fprintf(fptr, "%s\n", to_write);
    }

### Screenshot hasil No3
Isi dari directory 'darat'

![](soal3/dir_darat.jpg)

Isi dari directory 'air'

![](soal3/dir_air.jpg)

Isi dari list.txt

![](soal3/list.jpg)

## Revisi no.3
Isi dari list txt seharusnya hanya berisi hewan 'air', tetapi terdapat kesalahan yaitu nama file list.txt termasuk ke dalamnya. 

Ditambahkan code berikut untuk hanya memasukkan file jpg saja

    char *ada;
    ada = strstr(dp->d_name, "jpg");
    if (ada)
	    fprintf(fptr, "%s\n", to_write);
    }

Sesudah direvisi

![](soal3/list_revisi.jpg)























