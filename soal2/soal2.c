#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void unzip(char *path, char *zipFile) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"unzip", "-d", path, zipFile, NULL};
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeDir(char *path, char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
	
		char pathDir[100];
		sprintf(pathDir, "%s%s", path, dir);
	
		char *argv[] = {"rm", "-r", pathDir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeFile(char *dir, char *fileName) 
{
	char source[100];
	strcpy(source, dir);
	strcat(source, fileName);

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", source, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void copy(char *src, char *nameOld, char *nameNew, char *dest) 
{
	char source[100];
	sprintf(source, "%s%s", src, nameOld);
	
	char target[100];
	sprintf(target, "%s%s/%s.png", src, dest, nameNew);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"cp", source, target, NULL};
		execv("/bin/cp", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void makeTxtFile(char *path, char *category, char *name, char *year) {
    char filePath[100];
    sprintf(filePath, "%s%s/data.txt", path, category);

    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id == 0) {
        FILE *writef;
        writef = fopen(filePath, "a");

        fprintf(writef, "%s;%s\n", name, year);

        fclose(writef);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        while(wait(&status) > 0);
    }
}

void sortTxtFile(char *path, char *dir) {
    pid_t child_id;
    child_id = fork();
    
    char sortCommand[200];
    sprintf(sortCommand, "sort -t';' -k2 %s%s/data.txt > %s%s/sorted.txt", path, dir, path, dir);

    if (child_id == 0) {
        char *argv[] = {"sh", "-c", sortCommand, NULL};
        execv("/bin/sh", argv);

        exit(EXIT_SUCCESS);
    } else if (child_id > 0) {
        wait(NULL);
    }
}

void refactorTxtFile(char *path, char *name){
    FILE *readf, *writef;
    
    char readPath[100];
    char writePath[100];
    
    sprintf(readPath, "%s%s/sorted.txt", path, name);
    sprintf(writePath, "%s%s/data.txt", path, name);
    
    readf = fopen(readPath, "r");
    writef = fopen(writePath, "w");

    fprintf(writef, "kategori : %s\n", name);

    char line[100];

    while(fgets(line, sizeof(line), readf)){
            char *breaks = strtok(line, ";");
            fprintf(writef,"\nnama : %s", breaks);

            breaks = strtok(NULL, ";");
            fprintf(writef,"\nrilis : tahun %s", breaks);
    }

    fclose(readf);
    fclose(writef);

	char removePath[100];
	sprintf(removePath, "%s%s/", path, name);
    removeFile(removePath, "sorted.txt");
}

void categorize(char *path, char *fileName) {
	char name[50];
	sprintf(name, "%s", fileName);
	
	char *token;
	char *data[3];
	token = strtok(fileName, ";_");
	
	while (token != NULL) {
		for (int i = 0; i < 3; ++i) {
			data[i] = token;
			token = strtok(NULL, ";_");
		}
		
		char *judul = data[0];
		char *tahun = data[1];
		char *kategori = data[2];
		
		// hapus ".png" dari kategori
        char *category;
        category = strstr(kategori, ".png");
        if (category != NULL) {
            int pos = category - kategori;
            sprintf(kategori, "%.*s", pos, kategori);
        }
        
        char dirPath[100];
        sprintf(dirPath, "%s%s", path, kategori);

		makeDir(dirPath);
		copy(path, name, judul, kategori);
        makeTxtFile(path, kategori, judul, tahun);
	}
}


int main() {
	char *pathDrakor = "/home/cindi/shift2/drakor/";
	char *zipFile = "drakor.zip";
	
	makeDir(pathDrakor); 
	unzip(pathDrakor, zipFile);
	
	DIR *dp;
	struct dirent *dirp;
	
	dp = opendir(pathDrakor);
	while ((dirp = readdir(dp)) != NULL) {
	    if (strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0) {
			if (dirp->d_type == DT_DIR) {
				removeDir(pathDrakor, dirp->d_name);
			} else {
				categorize(pathDrakor, dirp->d_name);
			}
		}	    	 
	}
	closedir(dp);

	dp = opendir(pathDrakor);
	while ((dirp = readdir(dp)) != NULL) {
	    if (strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0) {
	    	if (dirp->d_type == DT_REG) {
	    		removeFile(pathDrakor, dirp->d_name);
	    	} else {
				sortTxtFile(pathDrakor, dirp->d_name);
				refactorTxtFile(pathDrakor, dirp->d_name);
	    	}
		}	    	 
	}
	closedir(dp);

	return 0;
}
